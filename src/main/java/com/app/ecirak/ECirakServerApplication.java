package com.app.ecirak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@SpringBootApplication
@EntityScan("com.app.ecirak.entity")
@EnableJpaRepositories(basePackages = "com.app.ecirak.repository")
public class ECirakServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ECirakServerApplication.class, args);
	}

	/*  @Bean
	    public WebMvcConfigurer corsConfigurer() {
	        return new WebMvcConfigurerAdapter() {
	            @Override
	            public void addCorsMappings(CorsRegistry registry) {
	                registry.addMapping("/greeting-javaconfig").allowedOrigins("http://localhost:8883");
	            }
	        };
	    }*/
	
	
}
