package com.app.ecirak.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.ecirak.entity.SUser;

@Repository
@Transactional
public interface SUserRepository extends JpaRepository<SUser, Integer> {
	List<SUser> findByType(String type);
	List<SUser> findByCity_id(Integer city_id);
	List<SUser> findByDistrict_id(Integer district_id);
	List<SUser> findByNeighborhood_id(Integer neighborhood_id);
	public SUser findOneByUsername(String username);
	List<SUser>  findByUsername(String username);
	
}
