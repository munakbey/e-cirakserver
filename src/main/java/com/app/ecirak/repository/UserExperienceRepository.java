package com.app.ecirak.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.ecirak.entity.UserExperience;

@Repository
@Transactional
public interface UserExperienceRepository extends JpaRepository<UserExperience, Integer> {
	
	List<UserExperience> findByUser_id(Integer user_id);
	
}
