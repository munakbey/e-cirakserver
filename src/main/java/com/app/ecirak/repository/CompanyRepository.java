package com.app.ecirak.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.app.ecirak.entity.Company;

@Repository
@Transactional
public interface CompanyRepository extends JpaRepository<Company, Integer> {
	List<Company> findByCity_id(Integer city_id);
	List<Company> findByDistrict_id(Integer district_id);
	List<Company> findByNeighborhood_id(Integer neighborhood_id);
	List<Company> findByName(String name);
}
