package com.app.ecirak.security;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.app.ecirak.entity.SUser;
import com.app.ecirak.repository.SUserRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	@Autowired
	private SUserRepository userDao;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	  @Bean
	  public BCryptPasswordEncoder passwordEncoder() {	    
	    return new BCryptPasswordEncoder();
	  };
	  
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		SUser user = userDao.findOneByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), passwordEncoder().encode(user.getPassword()),
				new ArrayList<>());
	}
/*	  @Bean
	  public BCryptPasswordEncoder passwordEncoder() {	    
	    return new BCryptPasswordEncoder();
	  };
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if ("usernamee".equals(username)) {
			return new User("usernamee", passwordEncoder().encode("password"),
					new ArrayList<>());   
		}else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}*/
	

}