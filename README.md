# e-cirak-server first commit !

CREATE USER db_user IDENTIFIED BY db_user;
ALTER USER db_user QUOTA unlimited ON SYSTEM;
GRANT CREATE SESSION, CONNECT, RESOURCE, DBA TO db_user;
GRANT CONNECT TO db_user;
GRANT UNLIMITED TABLESPACE TO db_user;

alter session set current_schema=db_user;
